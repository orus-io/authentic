.. _settings:

===============================
Works with authentic 2 settings
===============================

Local settings can be written in a python settings file.

The ``AUTHENTIC2_SETTINGS_FILE`` environment variable must contain
this file path so the authentic server can load it.

For example::

    export AUTHENTIC2_SETTINGS_FILE=/etc/authentic/settings.py
    authentic2-ctl runserver

This section has to be completed. If you need it now, say it on the issue tracker :

https://dev.entrouvert.org/issues/6047
